> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

## LIS_4368

## Lindsey Mabrey

### Assignment # Requirements:

*Sub-Heading:*

1. Distributed version control with git and bitbucket
2. java/jsp/servlet development installationg
3. chapter questions (ch 1-4)

#### README.md file should include the following items:

* screenshot of running java hello
* screenshot of running http://localhost.9999
* git commands with short descriptions
* bitbucket repo thinks (this assignment and completed tutorial)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- create an empty git repository
2. git status- tells you how much your project is progressing in comparison to your repository
3. git add- moves changes from the working directory to the staging area 
4. git commit- takes the staged snapshot and commits it to the project history
5. git push- moves a series of commits to remote repository to convientenly publish contributions
6. git pull- merges the file from bitbucket into your local repository
7. git branch- list, create, or delete branches

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](http://bitbucket.org/lm13d/lis4368a1/raw/master/images/jdk_install.png)


*Screenshot of running http://localhost:9999*:

![Tomcat Installation Screenshot](http://bitbucket.org/lm13d/lis4368a1/raw/master/images/tomcat.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/lm13d/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/lm13d/myteamquotes/ "My Team Quotes Tutorial")
